package com.smartnews.customtabsdemo.customtabs;

/**
 * A Callback for when the service is connected or disconnected. Use those callbacks to
 * handle UI changes when the service is connected or disconnected.
 */
public interface CustomTabsConnectionCallback {
    /**
     * Called when the service is connected.
     */
    void onCustomTabsConnected();

    /**
     * Called when the service is disconnected.
     */
    void onCustomTabsDisconnected();
}
