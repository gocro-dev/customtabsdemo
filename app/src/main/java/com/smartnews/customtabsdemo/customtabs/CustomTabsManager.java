package com.smartnews.customtabsdemo.customtabs;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.customtabs.CustomTabsCallback;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.util.Log;
import android.widget.Toast;

import java.util.List;


public class CustomTabsManager extends CustomTabsCallback implements ServiceConnectionCallback {
    private static final String TAG = "CustomTabsManager";

    private Activity mActivity;
    private CustomTabsSession mCustomTabsSession;
    private CustomTabsClient mClient;
    private CustomTabsServiceConnection mConnection;
    private CustomTabsConnectionCallback mConnectionCallback;

    private long mStartTime;

    public CustomTabsManager(Activity activity) {
        mActivity = activity;
    }

    /**
     * Binds the Activity to the Custom Tabs Service.
     */
    public void bindCustomTabsService(CustomTabsConnectionCallback connectionCallback) {
        if (mClient != null) return;

        String packageName = CustomTabsHelper.getPackageNameToUse(mActivity);
        if (packageName == null) return;

        mConnectionCallback = connectionCallback;
        mConnection = new ServiceConnection(this);
        CustomTabsClient.bindCustomTabsService(mActivity, packageName, mConnection);
    }

    /**
     * Unbinds the Activity from the Custom Tabs Service.
     * @param activity the activity that is connected to the service.
     */
    public void unbindCustomTabsService(Activity activity) {
        if (mConnection == null) return;
        activity.unbindService(mConnection);
        mClient = null;
        mCustomTabsSession = null;
        mConnection = null;
    }

    /**
     * @see {@link CustomTabsSession#mayLaunchUrl(Uri, Bundle, List)}.
     * @return true if call to mayLaunchUrl was accepted.
     */
    public boolean mayLaunchUrl(Uri uri, Bundle extras, List<Bundle> otherLikelyBundles) {
        if (mClient == null) return false;

        CustomTabsSession session = getSession();
        if (session == null) return false;

        return session.mayLaunchUrl(uri, extras, otherLikelyBundles);
    }

    /**
     * Opens the URL on a Custom Tab if possible. Otherwise fallsback to opening it on a WebView.
     *
     * @param uri the Uri to be opened.
     */
    public void openCustomTab(Uri uri, boolean useSession) {
        String packageName = CustomTabsHelper.getPackageNameToUse(mActivity);
        CustomTabsSession session;
        if (useSession) {
            session = getSession();
        } else {
            session = mClient.newSession(this);
        }
        CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder(session).build();
        customTabsIntent.intent.setPackage(packageName);
        customTabsIntent.launchUrl(mActivity, uri);
    }

    /**
     * Creates or retrieves an exiting CustomTabsSession.
     *
     * @return a CustomTabsSession.
     */
    private CustomTabsSession getSession() {
        if (mClient == null) {
            mCustomTabsSession = null;
        } else if (mCustomTabsSession == null) {
            mCustomTabsSession = mClient.newSession(this);
        }
        return mCustomTabsSession;
    }

    @Override
    public void onServiceConnected(CustomTabsClient client) {
        mClient = client;
        mClient.warmup(0L);
        if (mConnectionCallback != null) mConnectionCallback.onCustomTabsConnected();
    }

    @Override
    public void onServiceDisconnected() {
        mClient = null;
        mCustomTabsSession = null;
        if (mConnectionCallback != null) mConnectionCallback.onCustomTabsDisconnected();
    }

    @Override
    public void onNavigationEvent(int navigationEvent, Bundle extras) {
        super.onNavigationEvent(navigationEvent, extras);
        switch (navigationEvent) {
            case CustomTabsCallback.NAVIGATION_STARTED:
                Log.d(TAG, "navigation started");
                mStartTime = System.nanoTime();
                break;
            case CustomTabsCallback.NAVIGATION_FINISHED:
                Handler mainHandler = new Handler(mActivity.getMainLooper());
                Runnable myRunnable = new Runnable() {
                    @Override
                    public void run() {
                        long duration = System.nanoTime() - mStartTime;
                        Toast.makeText(mActivity, "Loading time: " + duration/1_000_000.0 + "ms", Toast.LENGTH_LONG).show();
                    }
                };
                mainHandler.post(myRunnable);
                break;
            default:
        }
    }
}
