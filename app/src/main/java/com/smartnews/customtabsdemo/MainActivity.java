package com.smartnews.customtabsdemo;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;

import com.smartnews.customtabsdemo.customtabs.CustomTabsConnectionCallback;
import com.smartnews.customtabsdemo.customtabs.CustomTabsHelper;
import com.smartnews.customtabsdemo.customtabs.CustomTabsManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CustomTabsConnectionCallback, ViewStub.OnClickListener {
    private static final String TAG = "MainActivity";
    private static final Uri url = Uri.parse("http://developer.android.com");
    private static final Uri anotherUrl = Uri.parse("http://www.google.com");

    CustomTabsManager customTabsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        customTabsManager = new CustomTabsManager(this);

        findViewById(R.id.openButton).setOnClickListener(this);
        findViewById(R.id.openAnotherButton).setOnClickListener(this);
        findViewById(R.id.openDeprioritizedButton).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        customTabsManager.bindCustomTabsService(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        callMayLaunchUrl();
    }

    @Override
    public void onCustomTabsConnected() {
        callMayLaunchUrl();
    }

    private void callMayLaunchUrl() {
        ArrayList<Uri> urlList = new ArrayList<>();
        urlList.add(anotherUrl);
        List<Bundle> bundleList = CustomTabsHelper.bundleListForMayLaunchUrl(urlList);
        boolean success = customTabsManager.mayLaunchUrl(url, null, bundleList);
        if (success) {
            Log.e(TAG, "mayLaunchUrl is successfully called");
        }
    }

    @Override
    public void onCustomTabsDisconnected() {

    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();

        switch (viewId) {
            case R.id.openButton:
                customTabsManager.openCustomTab(url, true);
                break;
            case R.id.openAnotherButton:
                customTabsManager.openCustomTab(url, false);
                break;
            case R.id.openDeprioritizedButton:
                customTabsManager.openCustomTab(anotherUrl, true);
                break;
            default:
        }
    }
}
